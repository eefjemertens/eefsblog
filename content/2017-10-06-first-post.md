---
title: First post
date: 2017-10-06
---
# Voorbeeld post

Dit is een voorbeel post voor de **workshop**.

## Done
Wat we hebben gedaan is: 
1. Gitlab aangemaakt
2. Hugo geforkt
3. Settings aangepast
4. Blogposts verwijderd
5. Nieuwe post online gezet

## En verder

Wat we nog moeten doen:
* Thema wijzigen
* Nog meer settings aanpassen
* Meer bloggen 
* _Iets wat cursief is_
